/*
 * This code gives information about battery level every 5mins;
 * If the battery level is lower tahn 15% it tells you to charge it and the LED will blink
 * It also connects the arduino to the Wifi AP which was my phone
*/
#include <LBattery.h>
#include <LWiFi.h>
#include <LWiFiClient.h>
#define WIFI_AP "LinkitOneWifi"
#define WIFI_PASSWORD ""
#define WIFI_AUTH LWIFI_OPEN  // choose from LWIFI_OPEN, LWIFI_WPA, or LWIFI_WEP.

char buff[256];

void setup() {
  // put your setup code here, to run once:
  Serial.begin(9600);
  LWiFi.begin();
  // keep retrying until connected to AP
  Serial.println("Connexion au point d'accès");
  while (0 == LWiFi.connect(WIFI_AP, LWiFiLoginInfo(WIFI_AUTH, WIFI_PASSWORD)))
  {
    delay(1000);
  }
  // keep retrying until connected to website
  printWifiStatus();
  pinMode(13,OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  sprintf(buff,"Niveau de batterie= %d %", LBattery.level() );
  Serial.println(buff);
  sprintf(buff,"Batterie en charge= %d",LBattery.isCharging() );
  Serial.println(buff);
  lowBattery();    
 delay(300000); //300 000 ms for 5mns
}


void lowBattery(){
  int level;
  level = LBattery.level();
  if (LBattery.level() <= 15){
    Serial.println("La batterie est faible. Veuillez la recharger");
    digitalWrite(13,HIGH);
        delay(750);
        digitalWrite(13,LOW);
        delay(750);
  }
  else {
      Serial.println("Niveau de batterie correcte");
  }
}

void printWifiStatus()
{
  // print the SSID of the network you're attached to:
  Serial.print("SSID: ");
  Serial.println(LWiFi.SSID());
  // print your WiFi shield's IP address:
  IPAddress ip = LWiFi.localIP();
  Serial.print("Adresse IP: ");
  Serial.println(ip);
  Serial.print("Masque de sous-réseau ");
  Serial.println(LWiFi.subnetMask());
  Serial.print("Passerelle IP: ");
  Serial.println(LWiFi.gatewayIP());
  // print the received signal strength:
  long rssi = LWiFi.RSSI();
  Serial.print("Force du signal (RSSI):");
  Serial.print(rssi);
  Serial.println(" dBm");
}


